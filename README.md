# README #

This document contains the scripts for **Stop Consolidation** applied to the Ector County data.

All packages that need to be installed are in the .venv folder. Read more about how to create and import virtual environments in VSCode [here](https://code.visualstudio.com/docs/python/environments).

